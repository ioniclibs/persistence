'use strict';

describe('Persistence', function() {

    var database, $rootScope;

    beforeEach(module('persistence.services'));


    beforeEach(inject(function(persistence, _$rootScope_, $q) {
        var name = "teste.db";
        var version = '1.0';
        var schema = [
            {
                name: 'usuario',
                columns: [
                    {name: 'id', type: 'integer primary key'},
                    {name: 'nome', type: 'text'},
                    {name: 'idade', type: 'integer'}
                ]
            }];

        database = persistence(name, version);
        $rootScope = _$rootScope_;

        spyOn(database, "open").andCallFake(function(){

            var deferred = $q.defer();

            mockOpenDatabase(name, version, 'db', 1024 * 1024 * 100, function(db){
                deferred.resolve(db);
            });

            return deferred.promise;
        });

        spyOn(database, 'create');

        database.addSchema(schema);
        database.create();

    }));

    describe('database', function(){

        it("o banco de dados foi iniciado", function(){
            expect(database.create).toHaveBeenCalled();
        });

        describe("query", function(){
            it ("está definido e é uma função", function(){
                expect(database.query).toBeDefined();
                expect(typeof database.query).toBe('function');
            });
        });

        describe("fetch", function(){

            it ("está definido e é uma função", function(){
                expect(database.fetch).toBeDefined();
                expect(typeof database.fetch).toBe('function');
            });
        });

        describe("fetchAll", function(){
            it ("está definido e é uma função", function(){
                expect(database.fetchAll).toBeDefined();
                expect(typeof database.fetchAll).toBe('function');
            });
        });
    });
});

