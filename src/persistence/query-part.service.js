angular.module('persistence.services').service("QueryPart", function(){

    var self = this;
    self.sql = "";
    self.values = [];

    self.where = function(coluna, condicao, valor){
        self.sql = self.sql + coluna + condicao + "?";
        self.values.push(valor);
        return self;

    };

    self.and = function(coluna, condicao, valor){
        self.sql = self.sql + " and ";
        return self.where(coluna, condicao, valor);
    };

});
