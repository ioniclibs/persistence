'use strict';

describe('', function() {

    var module;
    var dependencies;
    dependencies = [];

    var hasModule = function(module) {
        return dependencies.indexOf(module) >= 0;
    };

    beforeEach(function() {

        // Get module
        module = angular.module('persistence');
        dependencies = module.requires;
    });

    it('carregou o modulo de configuração', function() {
        expect(hasModule('persistence.config')).toBeDefined();
    });

    it('carregou o modulo de serviços', function() {
        expect(hasModule('persistence.services')).toBeDefined();
    });
});
