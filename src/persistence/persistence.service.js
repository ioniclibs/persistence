/**
 * Factory responsavel pela criação de uma instancia de banco de dados.
 */
angular.module('persistence.services').factory('persistence', function($q) {

    function Database(name, version) {

        var self = this;

        // Armazena as configurações do banco de dados.
        var databaseConfig = {
            name: name || 'app.db',
            version: version || '1.0',
            tables: []
        };

        // Instancia do banco de dados.
        self.db = null;

        /**
         * Abre ou cria uma instancia do banco de dados.
         * @returns {*} SQLite ou WebSQl Database
         */
        self.open = function () {
            var db = null;

            if (window.cordova) {
                db = window.sqlitePlugin.openDatabase({name: databaseConfig.name, androidDatabaseImplementation: 2}); //device
            } else {
                db = window.openDatabase(databaseConfig.name, databaseConfig.version, 'db', 1024 * 1024 * 100); // browser
            }

            return $q.when(db);
        };

        /**
         * Adicina um nodo schema na base.
         * @param schema
         */
        self.addSchema = function(schema){
            databaseConfig.tables.push(schema);
        };

        self.create = function () {
            if (self.db) {
                return $q.when(self.db);
            }
            return self.open().then(function(db){
                self.db = db;
                angular.forEach(databaseConfig.tables, function (table) {
                    var columns = [];

                    angular.forEach(table.columns, function (column) {
                        columns.push(column.name + ' ' + column.type);
                    });

                    var query = 'CREATE TABLE IF NOT EXISTS ' + table.name + ' (' + columns.join(',') + ')';
                    self.query(query);
                });
                return self.db;
            });
        };

        /**
         *
         * @param query
         * @param bindings
         * @returns {*}
         */
        self.query = function (query, bindings) {
            bindings = typeof bindings !== 'undefined' ? bindings : [];
            var deferred = $q.defer();

            self.db.transaction(function (transaction) {
                transaction.executeSql(query, bindings, function (transaction, result) {
                    deferred.resolve(result);
                }, function (transaction, error) {
                    deferred.reject(error);
                });
            });

            return deferred.promise;
        };

        /**
         *
         * @param query
         * @param rows
         * @returns {*}
         */
        self.batch = function (query, rows) {
            var deferred = $q.defer();

            var result = [];

            function processResult(data) {
                result.push(data);
                if (result.length == rows.length) {
                    deferred.resolve(result);
                }
            }

            self.db.transaction(function (transaction) {
                angular.forEach(rows, function (row) {
                    transaction.executeSql(query, row, function (transaction, result) {
                        processResult(result);
                    }, function (transaction, error) {
                        processResult(error);
                    });
                });
            });

            return deferred.promise;
        };

        /**
         *
         * @param result
         * @returns {Array}
         */
        self.fetchAll = function (result) {
            var output = [];

            for (var i = 0; i < result.rows.length; i++) {
                output.push(result.rows.item(i));
            }

            return output;
        };

        /**
         *
         * @param result
         * @returns {*}
         */
        self.fetch = function (result) {
            if (result.rows.length > 0)
                return result.rows.item(0);

            return null;
        };


        /**
         * @param transaction: Objeto de transação
         * @param tableName: Nome da tabela que o registro será inserido
         * @param dictionary: Dicionário de campos: { coluna1DaTabela1: valor1, coluna1DaTabela1: valor2, ... }
         * @param onSuccess: Callback em caso de sucesso
         * @param onError: Callback em caso de erro
         * */
        self.createRecord = function (transaction, tableName, dictionary, onSuccess, onError) {
            var fields = [], fieldValues = [], parameters = [];
            for (var key in dictionary) {
                fields.push(key);
                fieldValues.push(dictionary[key]);
                parameters.push('?');
            }

            var sql = 'insert into ' + tableName + '(';
            sql += fields.join(',');
            sql += ') values (';
            sql += parameters.join(',');
            sql += ')';
            transaction.executeSql(sql, fieldValues,
                function (transaction, result) {
                    if (onSuccess) {
                        onSuccess(result.insertId);
                    }
                }, function (transaction, error) {
                    onError(error);
                });
        };

        function buildWhereArguments(whereArgs, fieldValues) {
            if (!whereArgs.dictionary) {
                if (whereArgs.params && whereArgs.params.length) {
                    fieldValues.push.apply(fieldValues, whereArgs.params);
                }
                return whereArgs.sql;
            }
            var whereParameter = [];
            for (var whereKey in whereArgs.dictionary) {
                whereParameter.push(whereKey + " = ?");
                fieldValues.push(whereArgs.dictionary[whereKey]);
            }
            return whereParameter.join(' and ');
        }

        /**
         * @param transaction: Objeto de transação
         * @param tableName: Nome da tabela que o registro será atualizado
         * @param dictionary: Dicionário de campos: { colunaDaTabela1: valorAtualizacao1, coluna2DaTabela1: valorAtualizacao2, ... }
         * @param whereArgs: Objeto para a cláusula where. Exemplo:
         'where id = 1 and sucesso = true' = { dictionary: { id: 1, sucesso: true } }
         'where id = 2 or sucesso = false' = { sql: 'where id = ? or sucesso = ?', params: [2, false] }
         * @param onSuccess: Callback em caso de sucesso
         * @param onError: Callback em caso de erro
         * */
        self.updateRecord = function (transaction, tableName, dictionary, whereArgs, onSuccess, onError) {
            var fields = [], fieldValues = [];
            for (var key in dictionary) {
                fields.push(key + ' = ?');
                fieldValues.push(dictionary[key]);
            }

            var sql = 'update ' + tableName + ' set ';
            sql += fields.join(',');

            if (whereArgs) {
                sql += ' where ';
                sql += buildWhereArguments(whereArgs, fieldValues);
            }
            transaction.executeSql(sql, fieldValues, onSuccess, function (transaction, error) {
                onError(error);
            });
        };

        self.clearFields = function(data, fieldToIgnoreList) {
            Object.keys(data).forEach(function(field) {
                if (typeof(data[field]) === 'object' || fieldToIgnoreList.indexOf(field) > -1) {
                    return;
                }
                delete data[field];
            });
            return data;
        };
    }

    return function(name, version){
        return new Database(name, version);
    };
});
