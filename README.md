# persistence


module.run();

meuServicoPersistencia.


marcacaoPonto.controller("TesteController", function(persistencia){
    console.log("TesteController");

    persistencia.obterDados().then(function(){
       console.log("dados obtidos no controller");
    });

});





angular.module("exemplo",[]).service('persistencia', function($q, persistence){

    var self = this,
        schema = {
            name: 'marcacoes',
            columns : [
                {name: 'id', type: 'integer unique primary key'},
                {name: 'nome', type: 'text'},
                {name: 'usuario_id', type: 'integer'}
            ]
        };

    self.db = persistence('marcacao-ponto.db', '1.0');
    self.db.addSchema(schema);

    self.init = function(){
        return self.db.create();
    };

    self.obterDados = function() {
        //return self.init()
        //    .then(function () {
        return self.db.query("select * from usuario")
            .then(function (result) {
                return $q.when(self.db.fetch(result));
            });
    };
});



